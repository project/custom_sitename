
-- SUMMARY --

Customize sitename modules gives you the flexibility to change the site name for
specific content type or URLs.This module will also take care of URL validations
and alias.

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

To set the site name go to admin->configuration->System->Customize Sitename
